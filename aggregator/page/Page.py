from collections import OrderedDict


class Page(OrderedDict):

    def __hash__(self):
        return hash(self['url'])

    def __repr__(self):
        return self['url']

    def __eq__(self, other):
        return self['url'] == other['url']
