def should_swap(document, prev, base_lists):
    score = 0
    size = len(base_lists[0])
    for base_list in base_lists:
        try:
            document_position = base_list.index(document)
        except ValueError:
            document_position = size + 1
        try:
            prev_position = base_list.index(prev)
        except ValueError:
            prev_position = size + 1

        if prev_position > document_position:
            score += 1
        else:
            score -= 1

    return score > 0


def optimize(base_lists, aggregated_list, size=None):
    size = size or len(aggregated_list)
    for it in range(1, size):
        if should_swap(aggregated_list[it], aggregated_list[it - 1], base_lists):
            temp = aggregated_list[it - 1]
            aggregated_list[it - 1] = aggregated_list[it]
            aggregated_list[it] = temp

    return aggregated_list
