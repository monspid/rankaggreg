import copy
import re
import string
from collections import defaultdict, OrderedDict
from math import log
from urllib.parse import urlparse

import nltk

from aggregator import spam_reducer

# N               The number of items included in the final merged list
# Ntq             The number of items containing tq
# zc(i)           The ith zone of c (see Table 3)
# Wzc (i)         The weight of zc(i)
# f(c, tq, zc(i)) The number of occurrences (frequency) of tq in zc(i)
# m               The number of exploited component engines
# k               The number of items included in each component list
# c               A single result (document) in a component list
# ri(c)           The ranking of c in the ith component list
# n_c             The number of component lists containing c
# q               An arbitrary user query
# tq              A term of q
# Q               The number of terms of q

nltk.download('stopwords')
nltk.download('punkt')
stemmer = nltk.PorterStemmer()
stop = nltk.corpus.stopwords.words('english') + list(string.punctuation)

zone_weights = {
    'title': 10,
    'description': 3,
    'url': 5
}


def get_terms(text):
    return (stemmer.stem(token) for token in nltk.word_tokenize(text.lower()) if token not in stop)


def generate_document_zones_terms(document):
    document['terms'] = {}
    for zone, weight in zone_weights.items():
        text = document[zone]
        if zone == 'url':
            text = ' '.join(re.compile(r'[^a-zA-Z0-9]+', re.UNICODE).split(text)[1:])
        document['terms'][zone] = nltk.Counter(get_terms(text))


def get_weighted_term_frequency(document, tq):
    weighted_term_frequency = 0
    for zone, weight in zone_weights.items():
        weighted_term_frequency += zone_weights[zone] * document['terms'][zone][tq]
    return weighted_term_frequency


def get_domain(url):
    uri = urlparse(url)
    return f'{uri.netloc}'


def aggregate(base_lists, q='', geo_extensions=None, **_):
    if geo_extensions is None:
        geo_extensions = ['com']

    tqs = set(get_terms(q))
    Q = len(tqs)

    m = len(base_lists)
    k = len(base_lists[0])

    merged_list = {}
    n = defaultdict(lambda: 0)
    r = [defaultdict(lambda: k + 1) for _ in range(m)]
    Ntq = defaultdict(lambda: 0)
    Wzf = defaultdict(lambda: defaultdict(lambda: 0))
    acc = defaultdict(lambda: 0)

    for it_list, base_list in enumerate(base_lists):
        for it_document, document in enumerate(base_list, start=1):
            url = document['url']
            n[url] += 1
            r[it_list][url] = it_document
            acc[get_domain(url)] += 1

            if n[url] == 1:
                merged_list[url] = document
                generate_document_zones_terms(document)

                for tq in tqs:
                    weighted_term_frequency = get_weighted_term_frequency(document, tq)
                    Wzf[url][tq] = weighted_term_frequency

                    if weighted_term_frequency > 0:
                        Ntq[tq] += 1

    N = len(merged_list)

    score = calculate_documents_score(merged_list, N, Ntq, Q, Wzf, acc, geo_extensions, k, m, n, r, tqs)
    result = sorted(score.items(), key=lambda item: item[1], reverse=True)
    return [item[0] for item in result]


def calculate_documents_score(merged_list, N, Ntq, Q, Wzf, acc, geo_extensions, k, m, n, r, tqs):
    score = OrderedDict()
    for url, document in merged_list.items():
        K = sum(k + 1 - r[it][url] for it in range(m))
        R = m * log(n[url] * K)

        Z_Q = 0
        U = 1

        if tqs:
            Z = sum(log(N / Ntq[tq]) * Wzf[url][tq] for tq in tqs if Ntq[tq] > 0)
            domain = get_domain(url)
            geo_factor = 1.2 if any(domain.endswith(geo_extension) for geo_extension in geo_extensions) else 1
            U = geo_factor * log(10 * (2 * m - 1 + acc[domain]) / (2 * m))
            Z_Q = Z / Q

        score[document] = U * (R + Z_Q)
    return score


def aggregate_with_filtered_spam(base_lists, q='', geo_extensions=None, method='borda', kemeny_opt=True, **_):
    base_lists = copy.deepcopy(base_lists)
    base_lists = spam_reducer.reduce(base_lists, method, kemeny_opt=kemeny_opt)
    return aggregate(base_lists, q=q, geo_extensions=geo_extensions)
