from collections import defaultdict, OrderedDict

from munkres import Munkres

from aggregator import kemeny


def reduce(base_lists, method, kemeny_opt):
    agg_list, pages_positions = _METHODS[method](base_lists, kemeny_opt)
    reduced_base_lists = [OrderedDict() for _ in range(len(base_lists))]

    for final_position, page in enumerate(agg_list, start=1):
        for it, page_position in enumerate(pages_positions[page]):
            if page_position <= len(base_lists[0]):
                reduced_base_lists[it][page] = max(page_position, final_position)

    for it, reduced_base_list in enumerate(reduced_base_lists):
        base_lists[it] = [ranking_item[0] for ranking_item in
                          sorted(reduced_base_lists[it].items(), key=lambda item: item[1])]

    return base_lists


def aggregate_take_the_best(base_lists, kemeny_opt=False, **_):
    return _aggregate_take_the_best(base_lists, kemeny_opt)[0]


def _aggregate_take_the_best(base_lists, kemeny_opt=False):
    positions = get_document_positions(base_lists)
    ranking_dict = OrderedDict()
    for document, pos in positions.items():
        ranking_dict[document] = min(pos)

    result = sorted(ranking_dict.items(), key=lambda item: item[1])
    agg_list = [item[0] for item in result]
    if kemeny_opt:
        agg_list = kemeny.optimize(base_lists, agg_list)

    return agg_list, positions


def aggregate_borda(base_lists, kemeny_opt=False, **_):
    return _aggregate_borda(base_lists, kemeny_opt)[0]


def _aggregate_borda(base_lists, kemeny_opt=False):
    positions = get_document_positions(base_lists)
    ranking_dict = OrderedDict()
    for document, pos in positions.items():
        ranking_dict[document] = -sum(pos)

    result = sorted(ranking_dict.items(), key=lambda item: item[1], reverse=True)
    agg_list = [item[0] for item in result]
    if kemeny_opt:
        agg_list = kemeny.optimize(base_lists, agg_list)

    return agg_list, positions


def aggregate_sfo(base_lists, kemeny_opt=False, **_):
    return _aggregate_sfo(base_lists, kemeny_opt)[0]


def _aggregate_sfo(base_lists, kemeny_opt=False):
    positions = get_document_positions(base_lists)
    ranking_dict = OrderedDict()
    for document, base_positions in positions.items():
        ranking_dict[document] = [
            sum(
                abs(base_position / len(positions) - final_position / len(positions))
                for base_position in base_positions
            )
            for final_position in range(1, len(positions) + 1)
        ]

    scaled_foot_rule_matrix = list(ranking_dict.values())
    min_cost_flow_matrix = Munkres().compute(scaled_foot_rule_matrix)

    agg_list = [doc for _, doc in sorted(zip(min_cost_flow_matrix, ranking_dict.keys()), key=lambda pair: pair[0][1])]

    if kemeny_opt:
        agg_list = kemeny.optimize(base_lists, agg_list)

    return agg_list, positions


def get_document_positions(base_lists):
    max_size = len(base_lists[0])
    num_of_base_lists = len(base_lists)
    positions = defaultdict(lambda: [max_size + 1] * num_of_base_lists)
    for it_list, base_list in enumerate(base_lists):
        for it_document, document in enumerate(base_list, start=1):
            positions[document][it_list] = it_document
    return positions


_METHODS = {
    'borda': _aggregate_borda,
    'sfo': _aggregate_sfo,
    'ttb': _aggregate_take_the_best,
}

METHODS = {
    'borda': aggregate_borda,
    'sfo': aggregate_sfo,
    'ttb': aggregate_take_the_best,
}
