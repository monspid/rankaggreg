class RequestManagerConfig:
    DEFAULT_NUM = 10
    MIN_NUM = 5
    MAX_NUM = 30
    DEFAULT_METHOD = 'take_the_best'
    PAGE_DEFAULT = 1


class ScraperConfig:
    HEADERS = {
        'HTTP_USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0',
        'HTTP_ACCEPT': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'HTTP_ACCEPT_LANGUAGE': 'en-US,en;q=0.5',
        'HTTP_ACCEPT_ENCODING': 'gzip, deflate',
        'HTTP_CONNECTION': 'keep-alive'
    }
    SEARCH_ENGINE = None
    BASE = None
    QUERY = 'q'
    NUMBER_OF_RESULTS = 'n'
    START = 'start'


class ScraperYahooConfig(ScraperConfig):
    SEARCH_ENGINE = 'Yahoo'
    BASE = 'https://search.yahoo.com/search?'
    QUERY = 'p'
    NUMBER_OF_RESULTS = 'pz'
    START = 'b'


class ScraperBingConfig(ScraperConfig):
    HEADERS = {
        "Host": "www.bing.com",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive",
        "Referer": "http://www.bing.com/",
        "Cookie": "MUID=39D3A7CDBD3A67920636A9E6B93A6446; SRCHD=AF=NOFORM; SRCHUID=V=2&GUID=D3960D471FFD4143B6046100EE904095&dmnchg=1; SRCHUSR=DOB=20191207&T=1575753174000; _HPVN=CS=eyJQbiI6eyJDbiI6MiwiU3QiOjAsIlFzIjowLCJQcm9kIjoiUCJ9LCJTYyI6eyJDbiI6MiwiU3QiOjAsIlFzIjowLCJQcm9kIjoiSCJ9LCJReiI6eyJDbiI6MiwiU3QiOjAsIlFzIjowLCJQcm9kIjoiVCJ9LCJBcCI6dHJ1ZSwiTXV0ZSI6dHJ1ZSwiTGFkIjoiMjAxOS0xMi0xMFQwMDowMDowMFoiLCJJb3RkIjowLCJEZnQiOm51bGwsIk12cyI6MCwiRmx0IjowLCJJbXAiOjN9; MUIDB=39D3A7CDBD3A67920636A9E6B93A6446; ABDEF=MRNB=1575755434232&MRB=0; SRCHHPGUSR=CW=1853&CH=161&DPR=1&UTC=60&WTS=63711605012; _SS=SID=1003AB7DADD0661609BDA553AC6E6711&R=-1&RG=200&RP=-1&RD=0&RM=0&RE=0&HV=1576009128; _EDGE_S=SID=1003AB7DADD0661609BDA553AC6E6711; ASPSESSIONIDASARBRAR=HAOHNOBBJNJGLBMNJPCBICCE; _RwBf=s=70&o=18&g=0; ipv6=hit=1576011824026&t=4; ASPSESSIONIDAQRBADBT=OBGPILNCLLOCNFLDADCCEKNN; ASPSESSIONIDAQCSTTRS=LOABBJLBPJGLJEFHJCKCFDME",
        "Upgrade-Insecure-Requests": "1"
    }
    SEARCH_ENGINE = 'Bing'
    BASE = 'https://www.bing.com/search?'
    NUMBER_OF_RESULTS = 'count'
    START = 'first'

class ScraperAskConfig(ScraperConfig):
    HEADERS = {
        "Host": "www.ask.com",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Referer": "https://www.ask.com/web?o=312&l=dir&qo=serpSearchTopBox&q=joseph+antoine+ferdinand+plateau",
        "Cookie": 'uc=u=70E477C7-9D74-4FAA-8EB0-10F2C6502BD6&fv=1575736497628&lv=1576011316253&nv=172&sn=ask-sites-2827462965-4s67k&od=google.com&ok=-&w=1920&h=1080&cd=24&op=-; firstSession=1575736498611; _ga=GA1.2.1833311601.1575736499; _fbp=fb.1.1575736498807.725861032; _sp_id.489c=f3d06e82-7fc9-4dbb-aa1b-636f899d9871.1575736499.3.1576010734.1575753008.c3516065-f693-480c-ad34-740959f1e3d5; XQS=; googlepersonalization=OrNk8gOrNk8gAA; euconsent=BOrNk8gOrNk8gAKAbBENCyAAAAAtCAAA; crfgL0cSt0r=true; __gads=ID=e940bd4c924074bf:T=1575736502:S=ALNI_MbR6OUGPVLteOZrlt2npmhOZXyG1A; _uv=0.3062500000000001; user="o=312&l=dir"; ucs=s=173822083&sv=1576009711570&sd=none&sp=-&sk=-&sc=-&so=-&ap=-&am=-&an=-&ad=dirN&ag=-&ps=10&af=-; domain=www.ask.com; newSession=1; _sp_ses.489c=*; adDepth=2; ad_cnt=4; _cmpQcif3pcsupported=1; _gid=GA1.2.1272269084.1576009722; segid=utm_content=null&utm_source=null&utm_medium=null&segid=0; segid_fastly=segid=2; articlesViewed=3; firstPageView=1; OnePageArticle_Paid=3; decacopt={"brandSafety":{"adt":"veryLow","alc":"veryLow","dlm":"veryLow","drg":"veryLow","hat":"veryLow","off":"veryLow","vio":"veryLow"},"fr":"false","slots":{"Ask_Search_D300x250_1":{"id":"0037e3ca-1b8e-11ea-906e-98f2b3ea1c44","vw":["40"],"grm":["40","50"],"pub":["40"]}}}; pbblc=["appnexus"]; _gat_UA-64450717-1=1',
        "Upgrade-Insecure-Requests": "1",
        "TE": "Trailers"
    }
    SEARCH_ENGINE = 'Ask'
    BASE = 'https://www.ask.com/web?'
    PAGE = 'page'
