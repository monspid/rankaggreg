class ScraperException(Exception):
    def __init__(self, search_engine, error):
        super().__init__()
        self.search_engine = search_engine
        self.error = error

    def __str__(self):
        return f"Error occurred during scraping {self.search_engine}: {self.error}"
