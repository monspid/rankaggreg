from scraper import config
from scraper.Scraper import Scraper


class ScraperBing(Scraper):
    _CONFIG = config.ScraperBingConfig()

    def _get_all_containers(self, html):
        super()._get_all_containers(html)
        return html.findAll('li', {'class': 'b_algo'})

    @staticmethod
    def _get_header(container):
        return container.h2.a

    @staticmethod
    def _get_title(header):
        return header.text

    @staticmethod
    def _get_url(header):
        return header['href']

    def _get_description(self, container):
        description = container.find('div', {'class': 'b_caption'}).p.text
        return description
