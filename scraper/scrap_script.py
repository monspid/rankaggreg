import os
from collections import defaultdict

from scraper.ScraperAsk import ScraperAsk
from scraper.ScraperBing import ScraperBing
from scraper.ScraperGoogle import ScraperGoogle
from scraper.ScraperYahoo import ScraperYahoo

n = 30

with open('../queries.csv') as f:
    queries = list(f.readlines())

scrapers = {
    'ask': ScraperAsk(),
    'bing': ScraperBing(),
    'google': ScraperGoogle(),
    'yahoo': ScraperYahoo()
}
for search_engine, scraper in scrapers.items():
    results = defaultdict(list)
    for query in queries:
        query = query.strip()
        print(query)
        page_number = 1
        while len(results[query]) < n:
            documents = scraper.get_pages_list(
                query, n=n, page_number=page_number
            )
            if not documents:
                raise Exception('Not interesting query')
            for document in documents:
                if all(result['url'] != document['url'] for result in results[query]):
                    results[query].append(document)
            page_number += 1

        results[query] = results[query][:n]

    for query, pages in results.items():
        dirname = '../lists/base/' + '_'.join(query.split())
        os.makedirs(dirname, exist_ok=True)
        with open(f'{dirname}/{search_engine}.csv', 'w+') as f:
            for page in pages:
                line = f'{query}@@@{page["url"]}@@@{page["title"]}@@@'f'{page["description"]}'
                line = line.replace('\n', ' ').replace(';', ',').replace('"', '').replace('@@@', ';') + '\n'
                f.write(
                    line
                )
