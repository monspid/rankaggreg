from scraper import config
from scraper.Scraper import Scraper


class ScraperAsk(Scraper):
    _CONFIG = config.ScraperAskConfig()

    def _get_all_containers(self, html):
        super()._get_all_containers(html)
        return html.findAll('div', {'class': 'PartialSearchResults-item'})

    def _prepare_params(self, query, n, page_number):
        return {
            self._CONFIG.QUERY: query,
            self._CONFIG.PAGE: page_number
        }

    @staticmethod
    def _get_header(container):
        return container.find('div', {'class': 'PartialSearchResults-item-title'}).a

    @staticmethod
    def _get_title(header):
        return header.text

    @staticmethod
    def _get_url(header):
        return header['href']

    def _get_description(self, container):
        class_ = 'PartialSearchResults-item-abstract'
        description_container = container.find('p', class_=class_) or container.find('div', class_=class_)
        return description_container.text
