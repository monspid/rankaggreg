from scraper import config
from scraper.Scraper import Scraper


class ScraperYahoo(Scraper):
    _CONFIG = config.ScraperYahooConfig()

    def _get_all_containers(self, html):
        super()._get_all_containers(html)
        return html.findAll('div', {'class': 'algo-sr'})

    @staticmethod
    def _get_header(container):
        header_field = container.find('div', {'class': 'compTitle'})
        return header_field.find('a')

    @staticmethod
    def _get_title(header):
        return header.text

    @staticmethod
    def _get_url(header):
        return header['href']

    def _get_description(self, container):
        description = container.find('div', {'class': 'compText'}).p.text
        return description
