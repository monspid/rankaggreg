from googleapiclient.discovery import build


class ScraperGoogle:
    my_api_key = 'AIzaSyDy0E2Hl-rdfGSG_7rLXYYTNZLAFKWFwlg'
    my_cse_id = '015193294856918109498:lkjyuscrvej'

    def google_search(self, search_term, api_key, cse_id, **kwargs):
        service = build("customsearch", "v1", developerKey=api_key)
        return service.cse().list(q=search_term, cx=cse_id, **kwargs).execute()

    def get_pages_list(self, query, n, page_number):
        n = n if n <= 10 else 10
        result = self.google_search(query, self.my_api_key, self.my_cse_id, start=page_number * n + 1)
        return [
            {'title': item['title'], 'url': item['link'].replace('https://', 'http://'), 'description': item['snippet']}
            for item in result['items']
        ]
