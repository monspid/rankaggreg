from http import HTTPStatus
from urllib import parse

import requests
from bs4 import BeautifulSoup

from scraper import config
from scraper.exception.ScraperException import ScraperException


class Scraper:
    _CONFIG = config.ScraperConfig()

    def __repr__(self):
        return f"<{self.__class__.__name__}>"

    def get_pages_list(self, query, n, page_number):
        try:
            return self.__try_get_pages_list(n, page_number, query)
        except Exception as e:
            raise ScraperException(self._CONFIG.SEARCH_ENGINE, e)

    def __try_get_pages_list(self, n, page_number, query):
        pages_list = []
        html = self.__get_html(query, page_number, n)
        containers = self._get_all_containers(html)
        if self.__containers_not_empty(containers):
            for container in containers:
                try:
                    pages_list.append(self.__get_result(container))
                except:
                    pass
        return pages_list

    def __get_html(self, query, page_number, n):
        params = self._prepare_params(query, n, page_number)
        search_url = self.__get_search_url(params)
        request = requests.get(search_url, headers=self._CONFIG.HEADERS)

        if request.status_code != HTTPStatus.OK:
            raise Exception(
                f"Could not get results. Status code: {request.status_code}")

        return BeautifulSoup(request.text, 'html.parser')

    @staticmethod
    def __containers_not_empty(containers):
        return len(containers) > 1 if containers else False

    def _prepare_params(self, query, n, page_number):
        return {
            self._CONFIG.QUERY: query,
            self._CONFIG.NUMBER_OF_RESULTS: n,
            self._CONFIG.START: (page_number - 1) * n,
        }

    def __get_search_url(self, params):
        return self._CONFIG.BASE + parse.urlencode(query=params, doseq=True)

    def _get_all_containers(self, html):
        pass

    def __get_result(self, container):
        header = self._get_header(container)
        title = self._get_title(header)
        url = self._get_url(header)
        url = url.replace('https://', 'http://')
        description = self._get_description(container)

        return {'title': title, 'url': url, 'description': description}

    @staticmethod
    def _get_header(container):
        pass

    @staticmethod
    def _get_title(header):
        pass

    @staticmethod
    def _get_url(header):
        pass

    def _get_description(self, container):
        pass
