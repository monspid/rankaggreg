import csv
import os
from collections import defaultdict, Counter

from aggregator.page.Page import Page

dir_ = '../lists/base'
queries = os.listdir(dir_)
query_urls = defaultdict(set)
pages = dict()
for query in queries:
    subdir = os.path.join(dir_, query)
    base_lists = os.listdir(subdir)
    for base_list in base_lists:
        filename = os.path.join(dir_, query, base_list)
        with open(filename) as f:
            reader = csv.reader(f, delimiter=';')
            for row in reader:
                query_ = row[0]
                url_ = row[1]
                title_ = row[2]
                page = Page(url=url_, title=title_)
                query_urls[query_].add(page)
                pages[url_] = page
    try:
        with open(f'../lists/xls/{query}.csv', 'r') as f:
            reader = csv.reader(f, delimiter='\t')
            with open(f'../lists/ground_truth/{query}.csv', 'w') as out:
                for row in reader:
                    writer = csv.writer(out, delimiter='\t')
                    truth = Counter(row[1:])['PRAWDA'] >= 2
                    new_row = [query, 0, pages[row[0]], int(truth)]
                    writer.writerow(new_row)
    except Exception as e:
        print(e)
