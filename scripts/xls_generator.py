import csv
import os
from collections import defaultdict

from aggregator.page.Page import Page

dir_ = '../lists/base'
queries = os.listdir(dir_)
query_urls = defaultdict(set)

for query in queries:
    subdir = os.path.join(dir_, query)
    base_lists = os.listdir(subdir)
    for base_list in base_lists:
        filename = os.path.join(dir_, query, base_list)
        with open(filename) as f:
            reader = csv.reader(f, delimiter=';')
            for row in reader:
                query_ = row[0]
                url_ = row[1]
                title_ = row[2]
                query_urls[query_].add(Page(url=url_, title=title_))

for query, pages in query_urls.items():
    print(query)
    print('\n'.join(page["url"] for page in pages))
