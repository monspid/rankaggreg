import copy
import csv
import functools
import json
import os
from collections import defaultdict
from subprocess import Popen, PIPE

from aggregator import quadrank, spam_reducer
from aggregator.page.Page import Page

lists_dir = '../lists'
dir_ = f'{lists_dir}/base'
# simplified_queries = os.listdir(dir_)
simplified_queries = [
    'cybertruck_top_speed',
    'Hekla_number_of_eruptions_in_the_XX_century',
    'how_many_lifeboats_were_on_the_titanic',
    'how_many_miles_can_hummingbirds_fly_nonstop',
    'scottish_fold_lifespan',
    'sunken_US_nuclear_submarines',
    'the_first_monopoly_board_game_publication_date',
    'the_first_phone_with_a_touchscreen',
    'who_invented_the_self-winding_watch_mechanism',
]

precision_metrics = ['map', 'P.5,10,20,30']

base_methods = {
    'quadrank': quadrank.aggregate
}

for spam_reducer_method_name, spam_reducer_method_func in spam_reducer.METHODS.items():
    for kemeny_opt in range(2):
        # method_ = f'spam_{spam_reducer_method_name}_{kemeny_opt}'
        # base_methods[method_] = functools.partial(
        #     spam_reducer_method_func, method=spam_reducer_method_name, kemeny_opt=kemeny_opt
        # )

        method_ = f'quadrank_{spam_reducer_method_name}_{kemeny_opt}'
        base_methods[method_] = functools.partial(
            quadrank.aggregate_with_filtered_spam, method=spam_reducer_method_name, kemeny_opt=kemeny_opt
        )


def get_pseudo_methods(base_lists):
    pseudo_methods = {}
    for base_list_name, base_list in base_lists.items():
        pseudo_methods[f'base_{base_list_name}'] = functools.partial(pseudo_aggregate, base_list=list(base_list))
    return pseudo_methods


def pseudo_aggregate(base_list, *_, **__):
    return base_list


def get_query_and_base_lists(simplified_query):
    subdir = os.path.join(dir_, simplified_query)
    base_lists_files = os.listdir(subdir)
    base_lists = defaultdict(list)
    for base_list_file in base_lists_files:
        filename = os.path.join(dir_, simplified_query, base_list_file)
        with open(filename) as f:
            reader = csv.reader(f, delimiter=';')
            searcher = base_list_file.rsplit('.', maxsplit=1)[0]
            for row in reader:
                query = row[0]
                base_lists[searcher].append(Page(url=row[1], title=row[2], description=row[3]))

    return query, base_lists


def save_aggregated_list(aggregated_list, simplified_query, method):
    with open(get_aggregated_fname(simplified_query, method), 'w') as out:
        writer = csv.writer(out, delimiter='\t')
        for rank, result in enumerate(aggregated_list, 1):
            row = [simplified_query, 'Q0', result['url'], rank, len(aggregated_list) - rank, 'STANDARD']
            writer.writerow(row)


def get_aggregated_fname(simplified_query, method):
    dirname = f'{lists_dir}/aggregated/{simplified_query}'
    os.makedirs(dirname, exist_ok=True)
    return f'{dirname}/{method}.csv'


def get_trec_fname(simplified_query, method):
    dirname = f'{lists_dir}/trec/{simplified_query}'
    os.makedirs(dirname, exist_ok=True)
    return f'{dirname}/{method}.json'


def get_ground_truth_fname(simplified_query):
    return f'{lists_dir}/ground_truth/{simplified_query}.csv'


def trec_eval(simplified_query, method):
    trec_eval_cmd = '../trec_eval/trec_eval'
    ground_truth_file = get_ground_truth_fname(simplified_query)
    aggregated_file = get_aggregated_fname(simplified_query, method)
    trec_file = get_trec_fname(simplified_query, method)

    cmd_precisions = ' '.join(f'-m {metric.replace("_", ".")}' for metric in precision_metrics)
    trec_eval_cmd = f'{trec_eval_cmd} {cmd_precisions} {ground_truth_file} {aggregated_file}'

    output = Popen(trec_eval_cmd.split(' '), stdout=PIPE)
    response = output.communicate()[0].decode('utf-8')
    rows = response.split('\n')
    rows = [x.split() for x in rows if x]

    with open(trec_file, 'w') as outfile:
        json.dump({row[0]: float(row[2]) for row in rows}, outfile)


def main():
    for simplified_query in simplified_queries:
        query, base_lists = get_query_and_base_lists(simplified_query)

        methods = copy.deepcopy(base_methods)
        # methods.update(get_pseudo_methods(base_lists))

        for method_name, method_func in methods.items():
            print(simplified_query, method_name)
            aggregated_list = method_func(base_lists=list(base_lists.values()), q=query)
            save_aggregated_list(aggregated_list, simplified_query, method_name)
            trec_eval(simplified_query, method_name)


if __name__ == '__main__':
    main()
