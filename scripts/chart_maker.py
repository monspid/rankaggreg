import json
import os
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
from sklearn.preprocessing import normalize


def plot(data, rows, columns, title, second_title=''):
    cmap = LinearSegmentedColormap.from_list('', ['red', 'yellow', 'green'])

    fig, ax = plt.subplots()
    im = ax.imshow(data, cmap=cmap)

    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel(second_title, rotation=-90, va='bottom')

    ax.set_xticks(np.arange(len(columns)))
    ax.set_yticks(np.arange(len(rows)))
    ax.set_xticklabels(columns)
    ax.set_yticklabels(rows)

    plt.setp(ax.get_xticklabels(), rotation=90, ha='right', rotation_mode='anchor')

    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1] + 1) - .5, minor=True)
    ax.set_yticks(np.arange(data.shape[0] + 1) - .5, minor=True)
    ax.grid(which='minor', color='w', linestyle='-', linewidth=3)
    ax.tick_params(which='minor', bottom=False, left=False)

    for i in range(len(rows)):
        for j in range(len(columns)):
            ax.text(j, i, round(data[i, j], 3), ha='center', va='center')

    ax.set_title(title)
    width, height = fig.get_size_inches()
    fig.set_size_inches(width, height + 3.5)
    plt.savefig(f'../results/{title}.png')


lists_dir = '../lists'
trec_dir = f'{lists_dir}/trec'
trec = os.listdir(trec_dir)
query_results = defaultdict(dict)
method_results = defaultdict(dict)
precision_results = defaultdict(lambda: defaultdict(dict))
method_names = set()

for query_dir in trec:
    files_dir = f'{trec_dir}/{query_dir}'
    files = os.listdir(files_dir)
    for file_name in files:
        method = file_name.split('.json')[0]
        method_names.add(method)
        with open(f'{files_dir}/{file_name}') as f:
            trec_result = json.load(f)
            query_results[query_dir][method] = trec_result
            method_results[method][query_dir] = trec_result
            for precision_name, value in trec_result.items():
                precision_results[precision_name][query_dir][method] = value

method_names = list(sorted(method_names))
for precision_name, queries in sorted(precision_results.items()):
    data = [[value for method_name, value in sorted(method.items())] for query_name, method in sorted(queries.items())]
    data = np.array(data)
    normed_data = normalize(data, norm='l2')
    rows = [''.join([word[0] for word in query_name.split('_')]) for query_name in sorted(queries.keys())]
    plot(data=normed_data, rows=rows, columns=method_names, title=precision_name)
