import csv
import os
from collections import defaultdict
from subprocess import Popen, PIPE

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
from sklearn.preprocessing import normalize

from aggregator import quadrank
from aggregator.page.Page import Page
from aggregator.spam_reducer import _METHODS

dir_ = '../lists/base'
queries_dir = [
    'cybertruck_top_speed',
    'scottish_fold_lifespan',
    'sunken_US_nuclear_submarines',
    'the_first_phone_with_a_touchscreen',
    'who_invented_the_self-winding_watch_mechanism',
    'how_many_lifeboats_were_on_the_titanic'
]
trec_results = defaultdict(dict)
precision_metrics = ['map', 'P_5', 'P_10', 'P_20', 'P_30']
run_names = set()


def write_aggr(fname, res, query_dir):
    with open(fname, 'w') as out:
        for rank, result in enumerate(res, 1):
            writer = csv.writer(out, delimiter='\t')
            writer.writerow([query_dir, 'Q0', result['url'], rank, len(res) - rank, 'STANDARD'])


def plot(data, rows, columns, title, second_title=''):
    cmap = LinearSegmentedColormap.from_list('', ['red', 'yellow', 'green'])

    fig, ax = plt.subplots()
    im = ax.imshow(data, cmap=cmap)

    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel(second_title, rotation=-90, va='bottom')

    ax.set_xticks(np.arange(len(columns)))
    ax.set_yticks(np.arange(len(rows)))
    ax.set_xticklabels(columns)
    ax.set_yticklabels(rows)

    plt.setp(ax.get_xticklabels(), rotation=45, ha='right', rotation_mode='anchor')

    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1] + 1) - .5, minor=True)
    ax.set_yticks(np.arange(data.shape[0] + 1) - .5, minor=True)
    ax.grid(which='minor', color='w', linestyle='-', linewidth=3)
    ax.tick_params(which='minor', bottom=False, left=False)

    for i in range(len(rows)):
        for j in range(len(columns)):
            ax.text(j, i, round(data[i, j], 3), ha='center', va='center')

    ax.set_title(title)
    fig.tight_layout()
    # plt.show()
    plt.savefig(f'../results/{title}.png')


def trec_eval(aggregated, ground_truth, run_name):
    cmd = f'../trec_eval/trec_eval -m map -m P.5,10,20,30 ../lists/ground_truth/{ground_truth}.csv {aggregated}'
    output = Popen(cmd.split(' '), stdout=PIPE)
    response = output.communicate()[0].decode('utf-8')
    rows = response.split('\n')
    rows = [x.split() for x in rows if x]
    trec_results[ground_truth][run_name] = {r[0]: float(r[2]) for r in rows}


def main():
    for query_dir in queries_dir:
        subdir = os.path.join(dir_, query_dir)
        base_lists_files = os.listdir(subdir)
        base_lists = []
        for base_list_file in base_lists_files:
            filename = os.path.join(dir_, query_dir, base_list_file)
            with open(filename) as f:
                base_list = []
                reader = csv.reader(f, delimiter=';')
                for row in reader:
                    query = row[0]
                    base_list.append(Page(url=row[1], title=row[2], description=row[3]))
                base_lists.append(base_list)

        for method in _METHODS.keys():
            results = quadrank.aggregate(base_lists, q=query)
            run_name = '*quadrank'
            filename = f'../lists/aggregated/{query_dir}_{run_name}_res.csv'
            write_aggr(filename, results, query_dir)
            run_names.add(run_name)
            trec_eval(filename, query_dir, run_name)

            for kemeny_opt in range(2):
                results = quadrank.aggregate_with_filtered_spam(base_lists, q=query, method=method,
                                                                kemeny_opt=kemeny_opt)
                run_name = f'{method}_{kemeny_opt}'
                filename = f'../lists/aggregated/{query_dir}_{run_name}_res.csv'
                write_aggr(filename, results, query_dir)
                run_names.add(run_name)
                trec_eval(filename, query_dir, run_name)

    cols = sorted(list(run_names))

    for query, run in trec_results.items():
        data = np.array([[run[run_name][prc] for run_name in cols] for prc in precision_metrics])
        plot(data=data, rows=precision_metrics, columns=cols, title=query)

        # normed_data = np.array([[(record - mean(row)) / stdev(row) for record in row] for row in data])
        normed_data = normalize(data, norm='l2')

        plot(data=normed_data, rows=precision_metrics, columns=cols, title=query + 'norm')


if __name__ == '__main__':
    main()
